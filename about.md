---
layout: page
title: About
permalink: /about/
---

Recent graduate that studied BSc. (Hons) Games Design and Development with a keen interest in game development. Good knowledge of Unity 3D, HTML, CSS. Successfully launched two games on the Google play store.

### Contact me

[conorholmesdev@gmail.com](mailto:cpholmeswork@gmail.com)
