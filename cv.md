---
layout: page
title:  My Cv
permalink: /cv/
---
### Conor Holmes
Tipperary, Ireland

[My Linkedin](https://www.linkedin.com/in/conor-holmes-78a36580/)

[My Email](conorholmesdev@gmail.com).

[My Github Account](https://github.com/conorH22).

you can [ download CV ]({{ site.url }}/pdf/Conor Holmes CV.pdf)as PDF.

### Employment

##### May 2016 - September 2016 | Johnny Walsh Floor Coverings| | Full stack Web Developer 

- Worked as Web developer for Johnny Walsh Floor Coverings Ltd.
- Developed a company website to promote the company brand on the web.
- Developed technical and soft Skills (JavaScript, PHP, Html and CSS).
- Dealing with the Client with weekly meetings and communication between the web
hosting service.

[ work experience website](http://johnnywalshfloorcoverings.ie/).

### Education
##### Limerick Institution of Technology | 2012 – 2017 | B.Sc. (Hons.) in Games Design & Development | 2.1 

Successfully completed a BSc.(Hons.)Games Design & Development in achieving a Second-Class honours degree. The course had a focus on games design and development which allowed me to follow my passion in this area whist also building technical projects highly relevant in commercial and enterprise applications. This course allowed me to significantly develop my technical skills whilst also improving my teamwork and leadership skills through many team based projects.


### Projects

- Real-time Weather Application: developed a Real Time Weather system application in Cocos2d-x(C++) and Unity3D(C#), using Json data parsed from Wunderground Api. 
- Shopping cart system: developed a Java shopping cart that applies special offers and calculates total cost and tax. Tools used Java 8, JUnit, Eclipse and Git.
- PhoneBook Application: developed a phonebook application. Tools used Java 8, Spring, Maven, Eclipse and Git.
- Personal website : developed a responsive website to show my project work and personal blog. The website was developed using Html, Css, and Jekyll.

#### Relevant subjects include:

Advanced OO Programming,  Systems Analysis and Design, Computing Architecture, Database Systems,Software Security and Testing, Enterprise Application Development, Web development, Data Structures, Algorithms, Concurrent Programming, Network Programming.


##### 2007 – 2012 | St. Mary’s Secondary School, Newport | Leaving Certificate

### Skills and Certification

- Knowledge of Java, C++.
- Experienced with Google Analytics.
- Knowledge of  HTML , CSS, React, Angular and Javascript.
- Completed two Lynda courses in Git and Project Management.
- Excellent communication and presentation skills developed through major work and college based projects. 
- A large amount of team based project work in my employment together with college projects.


### Interests and Achievements

- Participant of various game jams and hackathons.
- Two popular Apps on the Google Play store.
- Active member of LIT Tipperary's Technology, Computer and Video Game Society.
- Event volunteer at Games Fleadh 2013-2017. 
- Learning about new software development and IT concepts.
- German: limited working proficiency.
- Interested in Swimming.



#### Contact me

[My Email](mailto:conorholmesdev@gmail.com)
